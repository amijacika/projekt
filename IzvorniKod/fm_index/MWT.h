#ifndef BWT_H_INCLUDED
#define BWT_H_INCLUDED
void bwtTransformacija(char *B, char *S, int *SA, int duljinaNiza);
#endif

struct node {
	struct node *zero;
	struct node *one;
	struct node *two;
	struct node *three;
	char *data;
	int position;
};
typedef struct node Node;

void insert(Node **current, char *data);
int buildRank(int position, Node * currentNode, char *code);
int rank(int position, char symbol, Node *rootNode);
