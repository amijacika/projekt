#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "Sais.h"
#include "BWT.h"
#include "MWT.h"

char dolar[5] = { '3', '3', '3', '3', '\0' };
char *abeceda;

int popuniSRang(int *sRang, unsigned char *S, int duljinaNiza, char *K);
int odrediOcc(char znak, int pozicija, unsigned char *B, int duljinaB);

/*
Program za trazenje pojavljivanja niza u podnizu, pomocu koristenja fm-indexa.
Program kao argument naredbenog retka prima putanju do datoteke s ulaznim nizom.
Ako je program pozvan bez argumenata putanja ce biti naknadno zatrazena.
Prvo se izgraduju strukture potrebne za provodenje algoritman.
Korisnik moze izvrsiti jedan ili vise upita predavajuci datoteku s upitom.

Program ispisuje broj pojavljivanja podniza te njegove pozicije u ulaznom nizu.
*/
int main(int argc, char** argv) {

	char *p;
	unsigned char *S, *B;
	int *SA;
	int i = 0;
	int znak, duljinaUpita;
	int duljinaNiza;
	clock_t pocetak, kraj;
	double vrijeme;
	int *sRang;
	Node *rootNode = NULL;
	int next = 0;
	int velicinaAbecede;
	FILE* datoteka;
	int neispravanUnos = 1;
	char imeDatoteke[500];

	int converted_number[64];
	int number_to_convert;
	int index;
	char base_digits[16] =
		{ '0', '1', '2', '3', '4', '5', '6', '7',
		'8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
	char **codes = (char**)malloc(256 * sizeof(char*));

	printf("*************Program za trazenje podnizova u nizu pomocu FM-indexa*****************\n\n\n");

	if (argc == 2) {
		datoteka = fopen(argv[1], "r");
		if (datoteka) {
			neispravanUnos = 0;
		}
	}

	while(neispravanUnos == 1) {
		printf("\nUnesite putanju do datoteku ciji sadrzaj zelite pretrazivati:\n");
		scanf("%s", imeDatoteke);
		datoteka = fopen(imeDatoteke, "r");
		if (!datoteka) {
			printf("Datoteka %s ne postoji!\n", imeDatoteke);
		}
		else {
			neispravanUnos = 0;
		}
	}

	fseek(datoteka, 0, SEEK_END);
	duljinaNiza = ftell(datoteka);

	//velicina polja mora biti veca za 1 zbog znaka kraja niza koji se dodaj u saisu
	SA = (int*)malloc((duljinaNiza + 1) * sizeof(int));
	S = (unsigned char*)malloc((duljinaNiza + 1) * sizeof(unsigned char));

	fseek(datoteka, 0, SEEK_SET);

	while ((znak = getc(datoteka)) != EOF) {
		if (znak != '\r\n' && znak != '\n' && znak != '\r') {
			S[i] = znak;
			i++;
		}
	}
	duljinaNiza = i + 1;
	fclose(datoteka);
	
	printf("\nDatoteka uspjesno ucitana.\n");
	neispravanUnos = 1;

	pocetak = clock();

	stvoriSa(S, SA, 0, duljinaNiza, 256);

	B = (unsigned char*)malloc((duljinaNiza)* sizeof(char));

	bwtTransformacija(B, S, SA, duljinaNiza);

	char *K = (char*)malloc(256 * sizeof(char));

	sRang = (int*)malloc(256 * sizeof(int));

	velicinaAbecede = popuniSRang(sRang, S, duljinaNiza, K);

	for (i = 0; i < strlen(abeceda); i++) {
		codes[i] = (char*)malloc(5 * sizeof(char));
		index = 0;
		number_to_convert = abeceda[i];
		while (number_to_convert != 0) {
			converted_number[index] = number_to_convert % 4;
			number_to_convert = number_to_convert / 4;
			++index;
		}
		index--;
		int max = index;

		for (; index >= 0; index--)
		{
			codes[i][max - index] = base_digits[converted_number[index]];
		}
		codes[i][4] = '\0';
	}
	codes[strlen(abeceda)] = (char*)malloc(5 * sizeof(char));
	codes[strlen(abeceda)] = dolar;

	for (i = 0; i < duljinaNiza; i++) {
		next = 0;
		for (int j = 0; j < strlen(abeceda); j++) {
			if (B[i] == abeceda[j]) {
				insert(&rootNode, codes[j]);
				next = 1;
			}
		}
		if (!next) {
			insert(&rootNode, codes[strlen(abeceda)]);
		}
	}
	printf("\n");
	kraj = clock();
	vrijeme = ((double)(kraj - pocetak)) / CLOCKS_PER_SEC;
	printf("\nVrijeme izgradnje struktura: %f sekundi.\n\n", vrijeme);
	
	int j;
	int count;
	clock_t pocetakUpit, krajUpit;
	int upit = 1;
	float vrijemeUpit = 0;

	do{
		while (neispravanUnos == 1) {
			printf("\nUnesite putanju do datoteke s podnizom:\n");
			scanf("%s", imeDatoteke);
			datoteka = fopen(imeDatoteke, "r");
			if (!datoteka) {
				printf("\nDatoteka %s ne postoji!\n", imeDatoteke);
			}
			else {
				neispravanUnos = 0;
			}
		}
		neispravanUnos = 1;

		fseek(datoteka, 0, SEEK_END);
		duljinaUpita = ftell(datoteka);
		fseek(datoteka, 0, SEEK_SET);

		p = (char*)malloc(duljinaUpita * sizeof(char));
		i = 0;

		while ((znak = getc(datoteka)) != EOF) {
			if (znak != '\r\n' && znak != '\n' && znak != '\r') {
				p[i] = znak;
				i++;
			}
		}
		duljinaUpita = i;
		fclose(datoteka);

		printf("\nDatoteka s podnizom uspjesno ucitana.\n");
		//mjerenje trajanja upita
		pocetakUpit = clock();

		i = duljinaUpita - 1;
		char c = p[i];
		int Lp = sRang[c] + 1;
		int br = 0;
		while (K[br] != c) {
			br++;
		}
		br++;
		int Rp;
		if (br == velicinaAbecede) {
			int brojIstih = 0;
			for (j = 0; j < duljinaNiza; j++) {
				if (S[j] == K[br - 1]) {
					brojIstih++;
				}
			}
			Rp = Lp + brojIstih - 1;
		}
		else {
			Rp = sRang[K[br]];
		}

		while ((Lp <= Rp) && (i >= 1)) {
			c = p[i-1];
			//Lp = sRang[c] + rank(Lp - 1, c, rootNode) + 1;
			//Rp = sRang[c] + rank(Rp, c, rootNode);
			Lp = sRang[c] + odrediOcc(c,Lp -1, B, duljinaNiza) + 1;
			Rp = sRang[c] + odrediOcc(c, Rp, B, duljinaNiza);
			i--;
		}
		if (Rp < Lp) {
			count = 0;
		}
		else {
			count = Rp - Lp + 1;
		}
		krajUpit = clock();
		vrijemeUpit = ((double)(krajUpit - pocetakUpit)) / CLOCKS_PER_SEC;

		printf("\nBroj pojavljivanja podniza je:%d\n\n", count);

		if (count != 0) {
			printf("Pocetak podniza u ulaznom nizu pojavljuje se na pozicijama:\n\n");
			for (i = 0; i < count; i++) {
				if (i % 5 == 0) {
					printf("\n");
				}
				printf("[%d]", SA[Lp + i - 1]);
			}
		}
		printf("\n\nTrajanje upita je:%f sekundi.\n\n*******************************\n", vrijemeUpit);
		printf("Unesite 1 za izlaz ili bilo koji drugi znak ako zelite pretrazivati s novim podnizom:\n");

		scanf("%c%c", &c, &c);
		free(p);

		if (c == '1') {
			upit = 0;
		}

	} while (upit == 1);

	free(S);
	free(SA);
	free(B);
	
	return 0;
}

/*
Polje sRang mora biti velicine 256, vraca velicinu abecede.
Popunjava polje sRang za svaki znak abecede s brojem leksikografski manjih znakova abecede iz ulaznog niza.
*/
int popuniSRang(int *sRang, unsigned char *S, int duljinaNiza, char *K) {
	int i;
	int j;
	int agregacijaS = 0;
	int pom;
	int velKa = 0;
	int novi;

	for (i = 0; i < 256; i++) {
		sRang[i] = 0;
	}

	for (i = 0; i < duljinaNiza; i++) {
		//dio za popunjavanje abecede
		novi = 1;
		for (j = 0; j < velKa; j++) {
			if (S[i] == K[j]) {
				novi = 0;
				break;
			}
		}
		if (novi == 1) {
			K[velKa] = S[i];
			velKa++;
		}
		sRang[S[i]]++;
	}
	abeceda = (char*)malloc((duljinaNiza + 1) * sizeof(char));
	for (i = 0; i < velKa; i++) {
		abeceda[i] = K[i];
	}
	qsort(K, velKa, 1, usporedi);

	for (i = 0; i < velKa; i++) {
		pom = agregacijaS;
		agregacijaS += sRang[K[i]];
		sRang[K[i]] = pom;
	}
	return velKa;
}

/*
Racuna broj pojavljivanja danog znaka u polju do dane pozicije.
*/
int odrediOcc(char znak, int pozicija, unsigned char *B, int duljinaB) {
	int occ = 0;
	int i;

	for (i = 0; i < pozicija; i++) {
		if (znak == B[i]) {
			occ++;
		}
	}
	return occ;
}
