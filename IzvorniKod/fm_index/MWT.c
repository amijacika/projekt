#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node {
	struct node *zero;
	struct node *one;
	struct node *two;
	struct node *three;
	char *data;
	int position;
};
typedef struct node Node;

char a[5];
char c[5];
char g[5];
char t[5];
char dol[5];

void insert(Node **current, char *data)  {
	Node *temp = NULL;
	if (*current == NULL) {
		temp = (Node *)malloc(sizeof(Node));
		temp->zero = temp->one = temp->two = temp->three = NULL;
		temp->data = (char *)malloc(6000000 * sizeof(char));
		*current = temp;
		(*current)->position = 0;
	}
/*	else {
		(*current)->data = (char*)realloc((*current)->data, ((*current)->position + 1) * sizeof(char));
	}
*/	(*current)->data[(*current)->position] = data[0];
	(*current)->data[(*current)->position+1] = '\0';
	(*current)->position++;

	
	if (strlen(data) > 1) {
		switch (data[0]) {
		case '0':
			insert(&(*current)->zero, &data[1]);
			break;
		case '1':
			insert(&(*current)->one, &data[1]);
			break;
		case '2':
			insert(&(*current)->two, &data[1]);
			break;
		case '3':
			insert(&(*current)->three, &data[1]);
		}
	}
//	printf("Trenutno polje: %s\n", (*current)->data);
}


int buildRank(int position, Node *currentNode, char *code) {
	int rank = -1;
	int i = 0;
	char znak = currentNode->data[0];
	if (currentNode == NULL) {
		printf("Nema tog znaka\n!");
		return 0;
	}
	while (znak != '\0') {
		i++;
		znak = currentNode->data[i];
	}
	int len = i;
	for (i = 0; i<len && i < position; i++) {
		if (currentNode->data[i] == code[0]) rank++;
	}
//	printf("Rank: %d\n", rank);
	if (strlen(code)>2) {
		switch (code[0]) {
		case '0':
			return buildRank(rank+1, currentNode->zero, &code[1]);
			break;
		case '1':
			return buildRank(rank+1, currentNode->one, &code[1]);
			break;
		case '2':
			return buildRank(rank+1, currentNode->two, &code[1]);;
			break;
		case '3':
			return buildRank(rank+1, currentNode->three, &code[1]);
			break;
		}
	}
	else return rank + 1;
}


int rank(int position, char symbol, Node *rootNode) {
//	printf("Znak %c, pozicija %d\n", symbol, position);
	if (symbol == 'A') return buildRank(position, rootNode, &a[0]);
	if (symbol == 'C') return buildRank(position, rootNode, &c[0]);
	if (symbol == 'G') return buildRank(position, rootNode, &g[0]);
	if (symbol == 'T') return buildRank(position, rootNode, &t[0]);
	if (symbol == (char)0) return buildRank(position, rootNode, &dol[0]);
}