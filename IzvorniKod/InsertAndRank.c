#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node { 
	char data[10]=0;
  	struct node *zero;
  	struct node *one;
  	struct node *two; 
  	struct node *three;
};
typedef struct node Node;
Node *rootNode = NULL;

char a[5] = "1001";
char c[5] = "1003";
char g[5] = "1013";
char t[5] = "1110";

void insert(Node *current, char *data)  {
	if(current == NULL) {
		current = (Node *) malloc (sizeof(Node));
	}
	current->data[strlen(current->data)] = data[0];
	current->data[strlen(current->data)] = '\0';
	if(strlen(data) > 1) {
		switch(data[0]) {
			case '0' :
				insert(current->zero, &data[1]);
				break;
			case '1' :
				insert(current->one, &data[1]);
				break;
			case '2' :
				insert(current->two, &data[1]);
				break;
			case '3' :
				insert(current->three, &data[1]);
		}
	}
}


int buildRank(int position, Node * currentNode, char *code) {
	int rank = 0;
	int i;
	for (i=0; i<strlen(currentNode->data); i++) {
		if (currentNode->data[i]==code[0]) rank++;
	}
	if (strlen(code)>1) {
		switch(code[0]) {
			case '0' :
				return buildRank (rank, currentNode->zero, &code[1]);
				break;
			case '1' :
				return buildRank (rank, currentNode->one, &code[1]);
				break;
			case '2' :
				return buildRank (rank, currentNode->two, &code[1]);;
				break;
			case '3' :
				return buildRank (rank, currentNode->three, &code[1]);
				break;
		}
	}
	else return rank;
}


int rank (int position, char symbol) {
	if (symbol == 'A') return buildRank(position, rootNode, &a[0]);
	if (symbol == 'C') return buildRank(position, rootNode, &c[0]);
	if (symbol == 'G') return buildRank(position, rootNode, &g[0]);
	if (symbol == 'T') return buildRank(position, rootNode, &t[0]);
}



int main(void) {
	char test[] = "CAAGTACTGA";
	int i=0;
	printf ("i=%d\n", i);
	for(i = 0; i < strlen(test); i++) {
		switch(test[i]) {
			case 'A' :
				insert(rootNode, &a[0]);
				break;
			case 'C' :
				insert(rootNode, &c[0]);
				break;
			case 'G' :
				insert(rootNode, &g[0]);
				break;
			case 'T' :
				insert(rootNode, &t[0]);
		}
		
	}	

	i=rank(5, 'A');
	printf ("i=%d\n", i);

	return 0;
}
