#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node { 
	char data[10];
  	struct node *zero;
  	struct node *one;
  	struct node *two; 
  	struct node *three;
};
typedef struct node Node;
Node *rootNode = NULL;
int rank;

char a[5] = "1001";
char c[5] = "1003";
char g[5] = "1013";
char t[5] = "1110";

void insert(Node *current, char *data)  {
	if(current == NULL) {
		current = (Node *) malloc (sizeof(Node));
	}
	current->data[strlen(current->data)] = data[0];
	if(strlen(data) > 1) {
		switch(data[0]) {
			case '0' :
				insert(current->zero, &data[1]);
				break;
			case '1' :
				insert(current->one, &data[1]);
				break;
			case '2' :
				insert(current->two, &data[1]);
				break;
			case '3' :
				insert(current->three, &data[1]);
		}
	}
}

int getSelect(Node *current, char *data) {
	char sign = data[0];
	int number;
	if(strlen(data) == 1)
		number = rank;
	else {
		switch(sign) {
			case '0' :
				number = getSelect(current->zero, &data[1]) + 1;
				break;
			case '1' :
				number = getSelect(current->one, &data[1]) + 1;
				break;
			case '2' :
				number = getSelect(current->two, &data[1]) + 1;
				break;
			case '3' :
				number = getSelect(current->three, &data[1]) + 1;
		}
	}
	for(int i = 0; i < strlen(current->data); i++) {
		if(current->data[i] == sign) {
			if(number == 1) 
				return i;
			else
				number--;
		}
	}
	
}

int select(int rankNumber, char charTarget) {
	rank = rankNumber;
	switch(charTarget) {
		case 'A' :
			return getSelect(rootNode, &a[0]);
		case 'C' :
			return getSelect(rootNode, &c[0]);
		case 'G' :
			return getSelect(rootNode, &g[0]);
		case 'T' :
			return getSelect(rootNode, &t[0]);
	}	
}


int main(void) {
	char test[] = "CAAGTACTGA";
	int i;
	for(i = 0; i < strlen(test); i++) {
		switch(test[i]) {
			case 'A' :
				insert(rootNode, &a[0]);
				break;
			case 'C' :
				insert(rootNode, &c[0]);
				break;
			case 'G' :
				insert(rootNode, &g[0]);
				break;
			case 'T' :
				insert(rootNode, &t[0]);
		}
	}

	printf("%d\n", select(2, 'A'));

	return 0;
}
