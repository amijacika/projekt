#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
//#include <iostream>
#include <math.h>

#include <divsufsort.h>

#include "Sais.h"
#include "SaisLite.h"
#include "saisOrig.h"
#include "saca-k.h"


int usporediSufiksnaPolja(int * SA1, int * SA2,int duljinaNiza,int brojOpcije);
void ispisOpcije(int i);
void generiranjeNizova(unsigned char *S, int duljinaNiza);
void pokreniAlgoritamIzgradnjeSA(unsigned char *S, int *SA1,int* SA2, int duljinaNiza,int i);

/*
Program za usporedu algoritama za izgradnju sufiksnog polja.
Podrzane su dvije mogucnosti, unos putanje do datoteke s nizom ili odabir slucajno generiranog niza velicine abecede 6.
Ne prima argumente naredbenog retka.
*/
int main(int argc, char** argv) {
	int i, ucitavanje, duljinaNiza;
	int *SA1;
	int *SA2;
	clock_t pocetak, kraj;
	double vrijeme = 0;
	/*double vrijemeTotal1 = 0;
	double vrijemeTotal2 = 0;*/
	int neispravanUnos;
	
	printf("VElicina int:%d", sizeof(int));
	printf("VElicina unsigned char:%d", sizeof(unsigned char));
	printf("VElicina void*:%d", sizeof(void*));

	//Odabir nacina ucitavanja niza
	do {
		printf("Unesite nacin ucitavanja niza:\n\t 1 - niz ucitan iz datoteke\n\t 2 - nasumicni nizovi\n");
		scanf("%d", &ucitavanje);
		if (ucitavanje != 1 && ucitavanje != 2) {
			printf("Neispravan unos\n");
		}
	} while (ucitavanje != 1 && ucitavanje != 2);
		//**UCITAVANJE NIZA IZ ZADANE DATOTEKE**//
	if (ucitavanje == 1) {
		unsigned char *S;
		int c;
		char imeDatoteke[500];
		FILE* ulazniNiz;

		do {
			neispravanUnos = 0;
			printf("\nUnesite putanju do datoteku ciji sadrzaj zelite ucitati:\n");
			scanf("%s", imeDatoteke);

			ulazniNiz = fopen(imeDatoteke, "r");
			if (!ulazniNiz) {
				printf("Datoteka %s ne postoji!\n", imeDatoteke);
				neispravanUnos = 1;
			}
		} while (neispravanUnos == 1);

		fseek(ulazniNiz, 0, SEEK_END);
		duljinaNiza = ftell(ulazniNiz);

		//velicina polja mora biti veca za 1 zbog znaka kraja niza koji se dodaj u saisu
		SA1 = (int*)malloc((duljinaNiza + 1) * sizeof(int));
		SA2 = (int*)malloc((duljinaNiza + 1) * sizeof(int));
		S = (unsigned char*)malloc((duljinaNiza + 1) * sizeof(unsigned char));

		if ((SA1 == NULL) || (SA2 == NULL) || (S == NULL)) {
			printf("\nNemoguce je alocirati memoriju!\n");
			exit(1);
		}

		fseek(ulazniNiz, 0, SEEK_SET);
		i = 0;
		while ((c = getc(ulazniNiz)) != EOF) {
			if (c != '\r\n' && c != '\n' && c != '\r') {
				S[i] = c;
				i++;
			}
		}
		fclose(ulazniNiz);
		duljinaNiza = i;

		for (i = 0; i <= 4; i++) {
			ispisOpcije(i);
			printf("Pocinje izgradnja SA:");
			pocetak = clock();
			pokreniAlgoritamIzgradnjeSA(S, SA1, SA2, duljinaNiza, i);
			kraj = clock();

			vrijeme = ((double)(kraj - pocetak)) / CLOCKS_PER_SEC;
			printf("\nVelicina: %d bajtova \nVrijeme izvodenja: %f sekundi.\n", duljinaNiza, vrijeme);

			//usporedivanje dobivena sufiksna polja drugih algoritama s nasim sufiksnim poljem SA1
			if (i > 0) {
				if (usporediSufiksnaPolja(SA1, SA2, duljinaNiza, i) == 0) {
					printf("Sufiksna polja nisu jednaka!\n");
				}
				else {
					printf("Sufiksna polja su jednaka!\n");
				}
			}
		}
		free(S);
		free(SA1);
		free(SA2);
		return 0;
	}
		//**UCITAVANJE NASUMICNIH NIZOVA**//
	if (ucitavanje == 2) {
			int  k, iter;
			unsigned char *S;
			double suma[4] = { 0, 0, 0, 0 };

			printf("\nUnesite zeljenu duljinu niza:\n");
			scanf("%d", &duljinaNiza);
			printf("\nUnesite zeljeni broj generiranih nizova\n");
			scanf("%d", &iter);

			SA1 = (int*)malloc((duljinaNiza + 1) * sizeof(int));
			SA2 = (int*)malloc((duljinaNiza + 1) * sizeof(int));
			S = (unsigned char*)malloc((duljinaNiza + 1) * sizeof(unsigned char));

			if ((SA1 == NULL) || (SA2 == NULL) || (S == NULL)) {
				printf("\nNemoguce je alocirati memoriju!\n");
				exit(1);
			}

			for (k = 0; k < iter; k++) {
				generiranjeNizova(S, duljinaNiza);
				printf("\n\n\t\t***Generiran je %d. niz****\n", k+1);
				for (i = 0; i <= 4; i++) {

					ispisOpcije(i);
					printf("Pocinje izgradnja SA:");
					pocetak = clock();
					pokreniAlgoritamIzgradnjeSA(S, SA1, SA2, duljinaNiza, i);
					kraj = clock();

					vrijeme = ((double)(kraj - pocetak)) / CLOCKS_PER_SEC;
					suma[i] += vrijeme;
					printf("\nVelicina: %d bajtova \nVrijeme izvodenja: %f sekundi.\n", duljinaNiza, vrijeme);

					//usporedivanje dobivena sufiksna polja drugih algoritama s nasim sufiksnim poljem SA1
					if (i > 0) {
						if (usporediSufiksnaPolja(SA1, SA2, duljinaNiza, i) == 0) {
							printf("Sufiksna polja nisu jednaka!\n");
						}
						else {
							printf("Sufiksna polja su jednaka!\n");
						}
					}
				}
			}
			printf("\nAlgoritmi su izvrcena %d puta s nizovima duljine %d.\nProsjecna vremena izvodenja su:\n", iter, duljinaNiza);
			for (i = 0; i <= 4; i++) {
				ispisOpcije(i);
				printf("%f", suma[i] / iter);
			}
			printf("\n");
			free(S);
			free(SA1);
			free(SA2);
			return 0;
		}
	}

	/*
	Usporeduje polja znak po znak.
	Vraca 1 ako su polja ista, inace 0.
	*/
	int usporediSufiksnaPolja(int * SA1, int * SA2, int duljinaNiza, int brojOpcije) {
		int i;
		//usporedba sa sufiksnim poljem originalnog sais algoritma
		if (brojOpcije == 2 || brojOpcije==3) {
			for (i = 0; i < duljinaNiza; i++) {
				if (SA1[i] != SA2[i]) {
					return 0;
				}
			}
		}
		//s ostalim algoritmima sufiksno polje usporedujemo od 1.og znaka (cime se preskoci indeks sentinela)
		else {
			for (i = 0; i < duljinaNiza; i++) {
				if (SA1[i + 1] != SA2[i]) {
					return 0;
				}
			}
		}
		return 1;
	}

	void ispisOpcije(int i) {
		printf("\n//////");
		switch (i) {
		case 0: {
			printf("Nas algoritam");
			break;
		}
		case 1: {
			printf("Sais-lite algoritam");
			break;
		}
		case 2: {
			printf("Originalni Sais algoritam");
			break;
		}
		case 3: {
			printf("SACA-K algoritam");
			break;
		}
		case 4: {
			printf("divsufsort algoritam");
			break;
		}
		}
		printf("//////\n");
	}

	/*
	Generira random nizove zeljene duljine. i sprema ih u polje S.
	Polje S mora biti iste velicine kao duljinaNiza
	*/
	void generiranjeNizova(unsigned char *S, int duljinaNiza) {
		int j, r;
		srand(time(NULL));
		for (j = 0; j < duljinaNiza; j++) {
			r = rand() % 6;
			switch (r) {
			case 0:
				*((char*)S + j) = 'A';
				break;
			case 1:
				*((char*)S + j) = 'C';
				break;
			case 2:
				*((char*)S + j) = 'G';
				break;
			case 3:
				*((char*)S + j) = 'T';
				break;
			case 4:
				*((char*)S + j) = 'R';
				break;
			case 5:
				*((char*)S + j) = 'K';
				break;
			}
		}
	}

	void pokreniAlgoritamIzgradnjeSA(unsigned char *S, int *SA1, int* SA2, int duljinaNiza, int i) {
		switch (i) {
		case 0: {
			stvoriSa(S, SA1, 0, duljinaNiza + 1, 256);
			break;
		}
		case 1: {
			sais(S, SA2, duljinaNiza);
			break;
		}
		case 2: {
			S[duljinaNiza] = 0;
			SA_IS(S, SA2, duljinaNiza + 1, 255, sizeof(char), 0);
			break;
		}
		case 3: {
			S[duljinaNiza] = 0;
			SACA_K(S, (unsigned int *)SA2, duljinaNiza + 1, 256, duljinaNiza + 1, 0);
			break;
		}
		case 4: {
			divsufsort(S, SA2, duljinaNiza);
			break;
	}
		}
	}
