#include<stdio.h>
#include<stdlib.h>

/*
Cuva vrijednost tipa znaka L_tip = 1, S_tip = 0
*/
typedef struct {
	unsigned char tip : 1;
} LStip;

static const LStip sTip = { 0 };
static const LStip lTip = { 1 };
/*
Znak koji se dodaje na kraj ulaznog nisa, dogovorno leksikografski najmanji znak
*/
static const unsigned char zadnjiZnak = (unsigned char)0;

void popuniT(LStip *T, void *S, int duljinaS, int razina);
int popuniAbecedu(void *S, unsigned char *K, int duljina, LStip *T, int *sRang, int velicinaAbecede);
int odrediLmsPodnizove(LStip *T, int *P, int duljinaS);
void induciranoSortirajLmsPodnizove(int *SA, void *S, unsigned char *K, LStip *T, int *P, int *B, int duljinaS, int velicinaAbecede, int *sRang, int *seRang, int brojLMS, int razina);
void inducirajSAizSA1(int *SA, void *S, int *SA1, LStip *T, unsigned char *K, int *P, int *B, int *sRang, int *seRang, int duljinaSA, int duljinaSA1, int velicinaAbecede, int razina);
int imenujNoveZnakove(int *SA, void *S, int *P, LStip *T, int duljinaS, int brojLMS, int razina);
void popuniRang(unsigned char *K, void *S, int *sRang, int *seRang, int velicinaAbecede, int duljinaS, int razina, int velicinaK);
void staviBPocetak(int *B, int *sRang, int velicinaAbecede, unsigned char *K, int razina);
void staviBKraj(int *B, int *seRang, int velicinaAbecede, unsigned char *K, int razina);
void induciranoLSort(void *S, int *SA, int *B, unsigned char *K, int *sRang, LStip *T, int duljinaS, int velicinaAbecede, int razina);
void induciranoSSort(void *S, int *SA, int *B, unsigned char *K, int *seRang, LStip *T, int duljinaS, int velicinaAbecede, int razina);
int usporediLMS(int index1, int index2, void *S, LStip *T, int duljinaS, int razina);
void manjakMemorije();

//komparator koristeno za sortiranje unsigned char abecede
int usporedi(const void *a, const void *b)
{
	return *(const unsigned char *)a - *(const unsigned char *)b;
}

/*
Ispisuje SA za provjeru
*/
void ispisiSA(int *SA, char *S, int duljinaS, int razina) {
	int i;
	printf("Razina:%d\nSA:", razina);
	if (razina == 0) {
		for (i = 0; i < duljinaS; i++) {
			printf("[%d]", SA[i]);
		}
		printf("\nS:");
		for (i = 0; i < duljinaS; i++) {
			printf("[%c]", S[i]);
		}
		printf("\n");
	}
	else {
		for (i = 0; i < duljinaS; i++) {
			printf("[%d]", SA[i]);
		}
		printf("\nS:");
		for (i = 0; i < duljinaS; i++) {
			printf("[%d]", *(S + i*sizeof(int)));
		}
		printf("\n");
	}
}

/*
Stvara sufiksno polje iz predanog niza S.
Polja S i SA moraju biti iste duljine te za 1 veca od originalnog ulaznog niza, polje S sadrzi ulazni niz iz kojeg se izgraduje SA(sufiksno polje),
pocetna razina je 0, duljina je duljina ulaznih polja, podrzava velicinu abecede do 256 znakova, velicinu abecede nije potrebno predavati.
*/
int stvoriSa(void *S, int *SA, int razina, int duljina, int velicinaAbecede) {
	unsigned char *K = 0;
	int *sRang;
	int *seRang;
	LStip *T;
	int *P;
	int *B;
	int brojLms;
	int i;
	void *S1;
	int *SA1;

	T = (LStip*)malloc(duljina * sizeof(LStip));
	if (T == NULL) {
		manjakMemorije();
	}

	if (razina == 0) {
		*((unsigned char*)S + duljina - 1) = zadnjiZnak;

		velicinaAbecede = 256;

		//pretpostavka da abeceda nije veca od 256 znakova
		K = (unsigned char*)malloc(velicinaAbecede * sizeof(unsigned char));
		if (K == NULL) {
			manjakMemorije();
		}

		B = (int*)malloc(velicinaAbecede * sizeof(int));
		if (B == NULL) {
			manjakMemorije();
		}

		sRang = (int*)malloc(velicinaAbecede * sizeof(int));
		if (sRang == NULL) {
			manjakMemorije();
		}

		seRang = (int*)malloc(velicinaAbecede * sizeof(int));
		if (seRang == NULL) {
			manjakMemorije();
		}

		int velicinaK = popuniAbecedu(S, K, duljina, T, sRang, velicinaAbecede);

		qsort(K, velicinaK, 1, usporedi);

		popuniRang(K, S, sRang, seRang, velicinaAbecede, duljina, razina, velicinaK);

		velicinaAbecede = velicinaK;

		P = (int*)malloc((duljina / 2) * sizeof(int));
		if (P == NULL) {
			manjakMemorije();
		}

		brojLms = odrediLmsPodnizove(T, P, duljina);

		P = (int*)realloc(P, brojLms * sizeof(int));

	}
	else {

		sRang = (int*)malloc(velicinaAbecede * sizeof(int));
		if (sRang == NULL) {
			manjakMemorije();
		}

		seRang = (int*)malloc(velicinaAbecede * sizeof(int));
		if (seRang == NULL) {
			manjakMemorije();
		}

		popuniRang(K, S, sRang, seRang, velicinaAbecede, duljina, razina, 0);
 
		P = (int*)malloc((duljina / 2) * sizeof(int));
		if (P == NULL) {
			manjakMemorije();
		}

		B = (int*)malloc(velicinaAbecede * sizeof(int));
		if (B == NULL) {
			manjakMemorije();
		}

		popuniT(T, S, duljina, razina);

		brojLms = odrediLmsPodnizove(T, P, duljina);

		P = (int*)realloc(P, brojLms * sizeof(int));
	}

	induciranoSortirajLmsPodnizove(SA, S, K, T, P, B, duljina, velicinaAbecede, sRang, seRang, brojLms, razina);

	int brojImena = imenujNoveZnakove(SA, S, P, T, duljina, brojLms, razina);

	S1 = SA;
	SA1 = SA + brojLms;


	if (brojImena < brojLms) {
		stvoriSa(S1, SA1, (razina + 1), brojLms, brojImena);
	}
	else {
		for (i = 0; i < brojLms; i++) {
			SA1[*((int*)S1 + i)] = i;
		}
	}
	inducirajSAizSA1(SA, S, SA1, T, K, P, B, sRang, seRang, duljina, brojLms, velicinaAbecede, razina);

	free(sRang);
	free(seRang);
	free(T);
	free(B);
	free(P);
	if (razina == 0) {
		free(K);
	}
	return 0;
}

/*
Prolazi niz S jednom s lijeva na desno i pounjava polje T ovisno o s ili l tipu znaka
*/
void popuniT(LStip *T, void *S, int duljinaS, int razina) {
	int i;
	//zadnji znak je uvijek s
	T[duljinaS - 1] = sTip;
	T[duljinaS - 2] = lTip;

	//ako je niz od samo jednog znaka da ne puca
	if (duljinaS == 1) {
		return;
	}
	if (razina == 0) {
		for (i = duljinaS - 3; i >= 0; i--) {
			if (*((unsigned char*)S + i) <= *((unsigned char*)S + i + 1)) {
				if (*((unsigned char*)S + i) < *((unsigned char*)S + i + 1)) {
					T[i] = sTip;
				}
				else {
					if (T[i + 1].tip == sTip.tip) {
						T[i] = sTip;
					}
					else {
						T[i] = lTip;
					}
				}
			}
			else {
				T[i] = lTip;
			}
		}
	}
	else {
		for (i = duljinaS - 3; i >= 0; i--) {
			if (*((int*)S + i) <= *((int*)S + i + 1)) {
				if (*((int*)S + i) < *((int*)S + i + 1)) {
					T[i] = sTip;
				}
				else {
					if (T[i + 1].tip == sTip.tip) {
						T[i] = sTip;
					}
					else {
						T[i] = lTip;
					}
				}
			}
			else {
				T[i] = lTip;
			}
		}
	}
}

/*
Stvara polje s abecedom, broji duljinu abecede, odreduje L/S tip znaka i broji brojnost svakog znaka iz abecede
Vraca duljinu abecede.
*/
int popuniAbecedu(void *S, unsigned char *K, int duljina, LStip *T, int *sRang, int velicinaAbecede) {
	int i;
	int j;
	int velKa = 0;

	//ako je niz od samo jednog znaka da ne puca
	if (duljina < 1) {
		return 0;
	}

	for (i = 0; i < velicinaAbecede; i++) {
		sRang[i] = 0;
	}

	//zadnji znak je uvijek s
	T[duljina - 1] = sTip;

	K[velKa] = *((unsigned char*)S + duljina - 1);
	sRang[*((unsigned char*)S + duljina - 1)]++;
	velKa++;

	int novi = 1;

	for (i = duljina - 2; i >= 0; i--) {
		//dio za odredivanje T
		if (*((unsigned char*)S + i) <= *((unsigned char*)S + i + 1)) {
			if (*((unsigned char*)S + i) < *((unsigned char*)S + i + 1)) {
				T[i] = sTip;
			}
			else {
				if (T[i + 1].tip == sTip.tip) {
					T[i] = sTip;
				}
				else {
					T[i] = lTip;
				}
			}
		}
		else {
			T[i] = lTip;
		}
		//dio za popunjavanje abecede
		novi = 1;
		for (j = 0; j < velKa; j++) {
			if (*((unsigned char*)S + i) == *(K + sizeof(unsigned char)*j)) {
				novi = 0;
				break;
			}
		}
		if (novi == 1) {
			*(K + sizeof(unsigned char)*velKa) = *((unsigned char*)S + i);
			velKa++;
		}
		//dio pripreme za rang, broj pojavljivanja
		sRang[*((unsigned char*)S + i)]++;
	}
	return velKa;
}

/*
Sprema u polje P sve indexe lms podnizova, koristi niz T za odredivanje istih
*/
int odrediLmsPodnizove(LStip *T, int *P, int duljinaS) {
	int brojLms = 0;
	int i;

	for (i = 1; i < duljinaS - 1; i++) {
		if (T[i - 1].tip == lTip.tip && T[i].tip == sTip.tip) {
			P[brojLms] = i;
			brojLms++;
		}
	}
	P[brojLms] = duljinaS - 1;
	brojLms++;
	return brojLms;
}

/*
Odreduje sRang i seRang od svakog znaka iz abeced i sprema u pripadajuca polja.
sRang je broj strogo manjih znakova u S a se broj manjih ili jednakih.
Polje sRang mora sadrzavati brojnost znakova abecede u ulaznom nizu. I polje K s abecedom mora biti sortirano.
Ovo se kasnije koristi u odredivanju pocetka ili kraja bucketa(kosare).
*/
void popuniRang(unsigned char *K, void *S, int *sRang, int *seRang, int velicinaAbecede, int duljinaS, int razina, int velicinaK) {
	int i;
	int agregacijaS = 0;
	int pom;

	//postavim sve na 0
	if (razina == 0) {
		for (i = 0; i < velicinaK; i++) {
			seRang[K[i]] = 0;
		}
	}
	else {
		for (i = 0; i < velicinaAbecede; i++) {
			sRang[i] = 0;
			seRang[i] = 0;
		}
	}

	if (razina == 0) {
		for (i = 0; i < velicinaK; i++) {
			pom = agregacijaS;
			agregacijaS += sRang[K[i]];
			sRang[K[i]] = pom;
			seRang[K[i]] = agregacijaS;
		}
	}
	else {
		//prvo broj pojavljivanja
		for (i = 0; i < duljinaS; i++) {
			sRang[*((int*)S + i)]++;
		}
		//agregacijsko odredivanje ranga
		for (i = 0; i < velicinaAbecede; i++) {
			pom = agregacijaS;
			agregacijaS += sRang[i];
			sRang[i] = pom;
			seRang[i] = agregacijaS;
		}
	}
}

/*
Stavlja u Bucket(B) indekse pocetka svakog bucketa
*/
void staviBPocetak(int *B, int *sRang, int velicinaAbecede, unsigned char *K, int razina) {
	int i;
	if (razina == 0) {
		for (i = 0; i < velicinaAbecede; i++) {
			B[K[i]] = sRang[K[i]];
		}
	}
	else {
		for (i = 0; i < velicinaAbecede; i++) {
			B[i] = sRang[i];
		}
	}
}

/*
Stavlja u Bucket(B) indekse na kraj svakog bucketa
*/
void staviBKraj(int *B, int *seRang, int velicinaAbecede, unsigned char *K, int razina) {
	int i;
	if (razina == 0) {
		for (i = 0; i < velicinaAbecede; i++) {
			B[K[i]] = seRang[K[i]] - 1;
		}
	}
	else {
		for (i = 0; i < velicinaAbecede; i++) {
			B[i] = seRang[i] - 1;
		}
	}
}

/*
Inducirano sortira lms podnizove.
*/
void induciranoSortirajLmsPodnizove(int *SA, void *S, unsigned char *K, LStip *T, int *P, int *B, int duljinaS, int velicinaAbecede, int *sRang, int *seRang, int brojLMS, int razina) {
	int i;
	unsigned char znakChr;
	int znak;

	//postavlja sve clanove SA na -1
	for (i = 0; i < duljinaS; i++) {
		SA[i] = -1;
	}

	staviBKraj(B, seRang, velicinaAbecede, K, razina);

	//stavlja lms podnizove na kraj pripadajuceg bucketa
	if (razina == 0) {
		for (i = 0; i < brojLMS; i++) {
			znakChr = *((unsigned char*)S + P[i]);
			SA[B[znakChr]] = P[i];
			B[znakChr]--;
		}
	}
	else {
		for (i = 0; i < brojLMS; i++) {
			znak = *((int*)S + P[i]);
			SA[B[znak]] = P[i];
			B[znak]--;
		}
	}
	induciranoLSort(S, SA, B, K, sRang, T, duljinaS, velicinaAbecede, razina);
	induciranoSSort(S, SA, B, K, seRang, T, duljinaS, velicinaAbecede, razina);
}

/*
Imenuje nove znakove(brojeve) za S1.
Vraca broj razlicitih znakova.
*/
int imenujNoveZnakove(int *SA, void *S, int *P, LStip *T, int duljinaS, int brojLMS, int razina) {
	int novoIme = 0;
	int indexProslog = -1;
	int i;
	int *Stemp;
	int lms = 0;

	//max n/2 duljina pomocnog polja ali obicno puno manje
	Stemp = (int*)malloc(brojLMS*sizeof(int));

	if (Stemp == NULL) {
		manjakMemorije();
	}
	for (i = 0; i < duljinaS; i++) {
		if ((SA[i] > 0) && (T[SA[i] - 1].tip == lTip.tip && T[SA[i]].tip == sTip.tip)) {
			Stemp[lms] = SA[i];
			lms++;
		}
	}

	for (i = 0; i < brojLMS; i++) {
		if (indexProslog == -1) {
			SA[Stemp[i]] = novoIme;
		}
		else {
			if (usporediLMS(indexProslog, Stemp[i], S, T, duljinaS, razina)) {
				SA[Stemp[i]] = novoIme;
			}
			else {
				novoIme++;
				SA[Stemp[i]] = novoIme;
			}
		}
		indexProslog = Stemp[i];
	}
	for (i = 0; i < brojLMS; i++) {
		SA[i] = SA[P[i]];
	}
	free(Stemp);
	return novoIme + 1;
}

/*
Uspoređuje dva lms podniza, ako su jednaki vraca 1 inace 0.
*/
int usporediLMS(int index1, int index2, void *S, LStip *T, int duljinaS, int razina) {
	int isti = 1;
	int prviL = 0;

	if (razina == 0) {
		while ((index1 < duljinaS) && (index2 < duljinaS)) {
			if (*((unsigned char*)S + index1) != *((unsigned char*)S + index2) || T[index1].tip != T[index2].tip) {
				isti = 0;
				break;
			}
			index1++;
			index2++;
			if (index1 < duljinaS) {
				if (T[index1].tip == lTip.tip) {
					prviL = 1;
				}
				if (prviL == 1 && T[index1].tip == sTip.tip) {
					if (index2 >= duljinaS) {
						isti = 0;
						break;
					}
					if (*((unsigned char*)S + index1) != *((unsigned char*)S + index2) || T[index1].tip != T[index2].tip) {
						isti = 0;
					}
					break;
				}
			}
		}
	}
	else {
		while ((index1 < duljinaS) && (index2 < duljinaS)) {
			if (*((int*)S + index1) != *((int*)S + index2) || T[index1].tip != T[index2].tip) {
				isti = 0;
				break;
			}
			index1++;
			index2++;
			if (index1 < duljinaS) {
				if (T[index1].tip == lTip.tip) {
					prviL = 1;
				}
				if (prviL == 1 && T[index1].tip == sTip.tip) {
					if (index2 >= duljinaS) {
						isti = 0;
						break;
					}
					if (*((int*)S + index1) != *((int*)S + index2) || T[index1].tip != T[index2].tip) {
						isti = 0;
					}
					break;
				}
			}
		}
	}
	return isti;
}

/*
Inducirano sortira sufikse.
*/
void inducirajSAizSA1(int *SA, void *S, int *SA1, LStip *T, unsigned char *K, int *P, int *B, int *sRang, int *seRang, int duljinaSA, int duljinaSA1, int velicinaAbecede, int razina) {
	int i;
	int *SATemp;
	unsigned char znakChr;
	int znak;

	SATemp = (int*)malloc(duljinaSA1 * sizeof(int));

	if (SATemp == NULL) {
		manjakMemorije();
	}

	for (i = 0; i < duljinaSA1; i++) {
		SATemp[i] = SA1[i];
	}

	for (i = 0; i < duljinaSA; i++) {
		SA[i] = -1;
	}

	staviBKraj(B, seRang, velicinaAbecede, K, razina);

	if (razina == 0) {
		for (i = duljinaSA1 - 1; i >= 0; i--) {
			znakChr = *((unsigned char*)S + P[SATemp[i]]);
			SA[B[znakChr]] = P[SATemp[i]];
			B[znakChr]--;
		}
	}
	else {
		for (i = duljinaSA1 - 1; i >= 0; i--) {
			znak = *((int*)S + P[SATemp[i]]);
			SA[B[znak]] = P[SATemp[i]];
			B[znak]--;
		}
	}

	free(SATemp);
	induciranoLSort(S, SA, B, K, sRang, T, duljinaSA, velicinaAbecede, razina);
	induciranoSSort(S, SA, B, K, seRang, T, duljinaSA, velicinaAbecede, razina);
}

/*
Inducirano sortira L-tipove, prolazeci kroz SA s lijeva na desno
*/
void induciranoLSort(void *S, int *SA, int *B, unsigned char *K, int *sRang, LStip *T, int duljinaS, int velicinaAbecede, int razina) {
	int i;
	staviBPocetak(B, sRang, velicinaAbecede, K, razina);
	int znak;

	if (razina == 0) {
		for (i = 0; i < duljinaS; i++) {
			if (SA[i] > 0) {
				znak = *((unsigned char*)S + SA[i] - 1);
				if (T[SA[i] - 1].tip == lTip.tip) {
					SA[B[znak]] = SA[i] - 1;
					B[znak]++;
				}
			}
		}
	}
	else {
		for (i = 0; i < duljinaS; i++) {
			if (SA[i] > 0) {
				znak = *((int*)S + SA[i] - 1);
				if (T[SA[i] - 1].tip == lTip.tip) {
					SA[B[znak]] = SA[i] - 1;
					B[znak]++;
				}
			}
		}
	}
}

/*
Inducirano sortira S-tipove, prolazeci kroz SA s desan na lijevo
*/
void induciranoSSort(void *S, int *SA, int *B, unsigned char *K, int *seRang, LStip *T, int duljinaS, int velicinaAbecede, int razina) {
	int i;
	staviBKraj(B, seRang, velicinaAbecede, K, razina);
	int znak;

	if (razina == 0) {
		for (i = duljinaS - 1; i >= 0; i--) {
			if (SA[i] > 0) {
				znak = *((unsigned char*)S + SA[i] - 1);
				if (T[SA[i] - 1].tip == sTip.tip) {
					SA[B[znak]] = SA[i] - 1;
					B[znak]--;
				}
			}
		}
	}
	else {
		for (i = duljinaS - 1; i >= 0; i--) {
			if (SA[i] > 0) {
				znak = *((int*)S + SA[i] - 1);
				if (T[SA[i] - 1].tip == sTip.tip) {
					SA[B[znak]] = SA[i] - 1;
					B[znak]--;
				}
			}

		}
	}
}

void manjakMemorije(){
	printf("\nNije moguce alocirati memoriju!!! Program se prekida.");
	exit(1);
}
