----------------------------------------------------------------------------------
			///FM-index///
///program za trazenje i brojanje ponavljanja podniza unutar zadanog niza///

Autori: Marko Franjic, Adriana Mijacika, Matea Sabolic, Tin Siroki
Datum:  19.01.2016.

Opis:	Ova mapa sadrzi program koji izgraduje strukturu FM-index pomocu
	koje se pretrazuje zadani niz. Uz to, sadrzi i program za usporedbu 
	vremena izgradnje sufiksnog polja (saisTest), koje je jedna od struktura
	koristenih u izgradnji FM-indexa.
-----------------------------------------------------------------------------------
Sadrzaj mape:
	IzvorniKod - mapa koja sadrzi kodove programa fm_index i saisTest
		     te makefile koji pokreæe kompajliranje oba programa	
	IzvrsniKod - mapa u koju se spremaju izvrsni programi fm_index i saisTest
	lib	   - mapa koja sadrzi libdivsufsort knjiznicu, koristenu unutar 
		     programa saisTest
	primjeri   - mapa s nekim testnim primjerima genoma koje smo koristili 
		     pri testiranju razvijenih programa (rezultati opisani u 
		     "Projekt_Tehnicka_dokumentacija")
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
		 //// UPUTE ZA IZGRADNJU IZVRSNIH DATOTEKA ////


Upute za kompajliranje i pokretanje programa fm_index i saisTest na linuxu:

1.) unutar komandne linije se pozicionirajte u mapu Izvornog koda
	../FM-index/IzvorniKod
2.) upisite make
3.) za pokretanje programa fm_index,odnosno saisTest upisite putanju do 
    izvrsnih datoteka koje se nalaze u mapi IzvrsniKod:
		../IzvrsniKod/FmIndex
		../IzvrsniKod/saisTest

Ako zelite kompajlirati samo jedan od programa fm_index, odnosno saisTest:

1.) unutar komandne linije se pozicionirajte u odgovarajuæu mapu unutar mape
    IzvorniKod
2.) upisite make
3.) pokrenite program upisivanje putanje do izvrsne datoteke (koja se nalazi
    unutar mape IzvrsniKod)


-------------------------------------------------------------------------------
------------------------------------------------------------------------------
				//FmIndex.exe//	
Nakon pokretanja programa korisnika se traži unos putanja do datoteka s ulaznim
nizom (neki primjeri se mogu naci u mapi Primjeri) te unos datoteke s upitom tj.
podnizom koji se zeli traziti unutar zadanog niza.
---------------------------------------------------------------------------------
				//saisTest.exe//
Program mjeri brzine nase implentacije sais algoritma te ju usporeduje s brzinama
ostalih implementacija algoritama preuzetih s interneta. Za unos niza korisniku se 
nudi da odabere nacin unosa ulaznog niza: putem datoteke ili random generirani niz
pri čemu korisnik odabire zeljenu duljinu tog niza te broj iteracija izvrsavanja 
algoritma kojii se zeli izvrsiti.


Popis algoritama koristenih u programu saisTest,te mjesta s kojih 
se navedeni algorimi mogu dohvatiti:		   

     sais:  https://sites.google.com/site/yuta256/sais
sais-lite:  https://sites.google.com/site/yuta256/sais
   saca-k:  https://code.google.com/p/ge-nong/downloads/detail?name=saca-k-tois-20130413.zip&can=2&q=
divsufsort: https://github.com/y-256/libdivsufsort/

--------------------------------------------------------------------------------
			//API sais//

/*
Stvara sufiksno polje iz predanog niza S.
Polja S i SA moraju biti iste duljine te za 1 veca od originalnog ulaznog niza,
polje S sadrzi ulazni niz iz kojeg se izgraduje SA(sufiksno polje),
pocetna razina je 0, duljina je duljina ulaznih polja, podrzava velicinu abecede do 256 znakova,
pravu velicinu abecede nije potrebno predavati. Izgradeno sufiksno polje sadrzi znak kraja niza("sentinel")
koje je uvijek na 0-toj poziciji ako znak kraja polja nije potreban za daljni rad koristiti polje od 1 znaka.
*/
int stvoriSa(void *S, int *SA, int razina, int duljina, int velicinaAbecede);

Jednostavan primjer koristenja:

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Sais.h"

int main() {
	// ulazni niz
	char *primjer = "ATTAGCGAGCG";
	int n = strlen(primjer);
	int i, j;

	//alocira potrbnu memoriju ***za jedan veca ulazna polja od primjer
	unsigned char *ulaz = (unsigned char*)malloc((n + 1) * sizeof(unsigned char));
	int *SA = (int*)malloc((n + 1) * sizeof(int));

	for (i = 0; i < n; i++) {
		ulaz[i] = primjer[i];
	}

	// izgradi SA
	stvoriSa(ulaz, SA, 0, n + 1, 0);

	// ispisi
	for (i = 0; i < n; ++i) {
		printf("SA[%2d] = %2d: ", i, SA[i]);
		for (j = SA[i]; j < n; ++j) {
			printf("%c", ulaz[j]);
		}
		printf("$\n");
		}

	// oslobodi memoriju
	free(SA);
	
	return 0;
}
---------------------------------------------------------------------------------




